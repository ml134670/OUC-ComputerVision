This file contains the input images used in the paper:

Shape and Materials by Example: A Photometric Stereo Approach
A. Hertzmann, S. M. Seitz. Proc. IEEE CVPR 2003. Madison,
Wisconsin. June 18-20, 2003.

http://grail.cs.washington.edu/projects/sam/


We hope you find these images to be useful.  Please include an
acknowledgment if you use them in a paper.



Copyright (c) 2003 Aaron Hertzmann and Steven M. Seitz