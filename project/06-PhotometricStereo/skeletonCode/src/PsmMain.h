#ifndef _PSM_MAIN_H_
#define _PSM_MAIN_H_

#include "PsmApp.h"
#include "PsmUI.h"

extern PsmApp *theApp;
extern PsmUI *theUI;

#endif // _PSM_MAIN_H_
